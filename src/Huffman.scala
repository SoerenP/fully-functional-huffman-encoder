/**
  * Created by soren on 28-12-15.
  */
import scala.collection.immutable.{BitSet, SortedSet}
import language.postfixOps

object Huffman {

  type CodeTable = Map[Char, BitSet]

  def weight(tree: HuffTree): Int = tree match {
    case Leaf(char,weight) => weight
    case Node(weight,left,right) => weight
  }

  def combine(left: HuffTree, right: HuffTree): HuffTree =
    Node(weight(left)+weight(right),left,right)

  def frequencies(chars: List[Char]): List[(Char, Int)] =
    chars.groupBy(w=>w).mapValues(_.size).toList

  def orderedLeaves(freq: List[(Char, Int)]): SortedSet[HuffTree] =
    SortedSet[HuffTree](freq.map(Function.tupled(Leaf)): _*)

  def combineTrees(trees: SortedSet[HuffTree]): HuffTree = trees match {
    case x if x.size < 2 => trees firstKey
    case x if x.size > 1 => combineTrees((trees drop 2)+combine(trees firstKey, (trees drop 1) firstKey))
  }

  def createCodeTree(chars: List[Char]): HuffTree =
    combineTrees(orderedLeaves(frequencies(chars)))

  def convert(tree: HuffTree, acc: BitSet, index: Int): CodeTable = tree match {
    case Leaf(char, int) => Map(char -> acc )
    case Node(weight,left,right) => convert(left,acc,index+1) ++ convert(right,acc.+(index),index+1)
  }

  def codeBits(table: CodeTable)(char: Char): BitSet =
    table.apply(char)

  def meldBitSets(bitsets: List[BitSet]): BitSet = bitsets match {
    case x if x.size < 2 => bitsets head
    case x if x.size > 1 =>
      meldBitSets((bitsets.head++bitsets.drop(1).head)::bitsets.drop(2))
  }

  def encode(tree: HuffTree)(text: List[Char]): List[BitSet] = {
    val table = convert(tree,BitSet(),0)
    text.map(x => codeBits(table)(x))
  }

  def getLetter(tree: HuffTree, bits: BitSet, index: Int): Char = tree match {
    case Leaf(char, weight) => char
    case Node(weight, left, right) =>
      if(bits.contains(index))
        getLetter(right,bits,index+1)
      else
        getLetter(left,bits,index+1)
  }

  def decode(tree: HuffTree, bits: List[BitSet]): List[Char] =
    {
      charAccumulator(tree,bits,List())
    }

  def charAccumulator(tree: HuffTree, bits: List[BitSet], acum: List[Char]): List[Char] = bits match {
    case Nil => acum
    case x :: tail =>
      val l = List(getLetter(tree,x,0))
      charAccumulator(tree, tail, acum ++ l)
  }

}
