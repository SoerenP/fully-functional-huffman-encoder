/**
  * Created by soren on 11-01-16.
  */


/*
* Find a proper testing framework for Scala. Or build your own!
 */

import Huffman._
import HuffTree._
import scala.collection.immutable.{BitSet, SortedSet}

object HuffTests {

  def main (args: Array[String]) = {
    val str = "abracadabra".toList

    println(testWeight() + "\n")
    println(testCombine() + "\n")
    println(testFrequencies(str) + "\n")
    println(testCombineTrees() + "\n")
    println(testCreateCodeTree(str) + "\n")
    println(testConvert(str) + "\n")
    println(testCodeBits(str) + "\n")
    println(testEncodingDecoding(str) + "\n")
  }

  def testWeight() = {
    val result = Huffman.weight(Node(9,Node(7,Leaf('c',3),Leaf('b',4)),Leaf('c',2)))
    val expected = 9
    println("Calculating weight")
    reportResult(expected,result)
    result == expected
  }

  def testCombine() = {
    val x = Node(9,Node(7,Leaf('c',3),Leaf('b',4)),Leaf('c',2))
    val y = Node(9,Node(7,Leaf('c',3),Leaf('b',4)),Leaf('c',2))
    val result = Huffman.combine(x,y)
    val expected =
      Node(18,Node(9,Node(7,Leaf('c',3),Leaf('b',4)),Leaf('c',2)),Node(9,Node(7,Leaf('c',3),Leaf('b',4)),Leaf('c',2)))

    println("Combining Nodes")
    println(x)
    println(y)
    reportResult(expected,result)
    result == expected
  }

  def testFrequencies(str: List[Char]) = {
    val expected: List[(Char,Int)] = List(('a',5),('b',2),('c',1),('r',2),('d',1))
    val result = Huffman.frequencies(str)
    println("Computing Frequencies")
    reportResult(expected,result)
    result == expected
  }

  def testCombineTrees() = {
    val x = Node(9,Node(7,Leaf('c',3),Leaf('b',4)),Leaf('c',2))
    val y = Node(9,Node(7,Leaf('c',3),Leaf('b',4)),Leaf('c',2))
    val z = Node(4,Node(2,Leaf('c',1),Leaf('b',1)),Leaf('y',2))
    val left = Huffman.combine(x,y)
    val right = Huffman.combine(z,z)
    val set = SortedSet[HuffTree]() + left + right
    val expected = Node(26,Node(8,Node(4,Node(2,Leaf('c',1),Leaf('b',1)),Leaf('y',2)),Node(4,Node(2,Leaf('c',1),
            Leaf('b',1)),Leaf('y',2))),Node(18,Node(9,Node(7,Leaf('c',3),Leaf('b',4)),Leaf('c',2)),Node(9,
            Node(7,Leaf('c',3),Leaf('b',4)),Leaf('c',2))))
    val result = combineTrees(set)
    println("Combining Trees")
    reportResult(expected,result)
    result == expected
  }

  def testCreateCodeTree(str: List[Char]) = {
    val result = createCodeTree(str)
    val expected = Node(11,Leaf('a',5),Node(6,Leaf('b',2),Node(4,Node(2,Leaf('d',1),Leaf('c',1)),Leaf('r',2))))
    println("Creating Code Tree")
    reportResult(result,expected)
    result == expected
  }

  def testConvert(str: List[Char]) = {
    val tree = createCodeTree(str)
    val result = convert(tree,BitSet(),0)
    val expected = Map('a' -> BitSet(), 'b' -> BitSet(0), 'c' -> BitSet(0,1,3), 'r' -> BitSet(0,1,2), 'd' -> BitSet(0,1))
    println("Converting Tree To Table")
    reportResult(expected,result)
    expected == result
  }

  def testCodeBits(str: List[Char]) = {
    val tree = createCodeTree(str)
    println(getLetter(tree,BitSet(0,1,3),0))
    println(BitSet(0,1,3).contains((2)))
    println(getLetter(tree,BitSet(0,1),0))
    val mapping = convert(tree,BitSet(),0)
    val result = codeBits(mapping)('c')
    val expected = BitSet(0,1,3)
    println("Coding Bits")
    reportResult(expected,result)
    expected == result
  }

  def testEncodingDecoding(str: List[Char]) = {
    println("Encoding and decoding " + str)

    val tree = createCodeTree(str)
    val encoding = encode(tree)(str)
    val decoding = decode(tree,encoding)

    println(str)
    println(encoding)
    println(decoding)
    str == decoding
  }

  def reportResult(expected: Any, result: Any) = {
    println("Expected")
    println(expected)
    println("Result")
    println(result)
    assert(expected == result)
  }

}
