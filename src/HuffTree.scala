/**
  * Created by soren on 28-12-15.
  */
import Huffman._

abstract class HuffTree
case class Node(weight:Int, left: HuffTree, right: HuffTree) extends HuffTree
case class Leaf(char: Char, weight: Int) extends  HuffTree

object HuffTree {
  implicit object HuffOrd extends  Ordering[HuffTree]{
    def compare(a: HuffTree, b:HuffTree) = {
      val comp = weight(a) compare weight(b)
      if(comp == 0) -1 else comp
    }
  }
}
