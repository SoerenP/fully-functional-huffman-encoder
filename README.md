###Fully Functional Huffman Encoder

This is a fully functional implementation of a Huffman Encoder in scala. As such, there is no mutable state, and no usage of
non-functional idioms such as "var" or "while".

All you need is the createCodeTree(), encode()() and decode()() functions, to create a HuffTree, and subsequently encode and decode strings using the created Tree!

Why functional you ask? To learn :) And the fully functional implementation can be extended quite easily to support partial or full persistence!


###Bugs/Missing Features:

As of now, the encoding and code tree generation only works on List[Char]. API should be added to allow encoding of entire files etc. The HuffTree could also be exportable, for easier repeated usage between work sessions. 


Nil input is not handled as of yet, so the above (working on files) could potentially fail for empty files!